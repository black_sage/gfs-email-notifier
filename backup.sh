#!/bin/bash

# Remote Destination
# The drive set in rclone.
REMOTE_DRIVE='gfs-google'
REMOTE_PARENT_DIR='gfs-data'

YEAR=$(date "+ %Y")
MONTH=$(date "+  %m")

# Local FILE
LFILE=$1

#rclone mkdir "${REMOTE_DRIVE}:${REMOTE_DIR}"

rclone copy --update --verbose --checkers 8 --contimeout 60s --timeout 300s --retries 3 --low-level-retries 10 "$LFILE" "${REMOTE_DRIVE}:${REMOTE_PARENT_DIR}/${YEAR}/${MONTH}" &> /dev/null


if [[ "${?}" -ne 0 ]]
then 
	echo "Error uploading ${LFILE} to ${REMOTE_DRIVE}" >&2
	exit 1
fi

# Generate Link for the uploaded file
link=$(rclone link "${REMOTE_DRIVE}:${REMOTE_PARENT_DIR}/${YEAR}/${MONTH}/${LFILE}")

python3 send_mail.py "${link}"


