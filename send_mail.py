import csv
import sys
import smtplib
from datetime import datetime
from email.message import EmailMessage

today = datetime.today().strftime('%Y-%m-%d')
tdate = datetime.today().strftime('%A %B %d, %Y')
EMAIL_ADDRESS = '' 
EMAIL_PASSWORD = ''

shared_link = sys.argv[1]

msg = EmailMessage()
msg['Subject'] = f"GFS Data for {today}"
msg['From'] = EMAIL_ADDRESS

# It is possible to send HTML Formatted Message
msg.set_content(f'GFS data for {today}, {shared_link}.')


# Load HTML formatted email 
with open('html-email.html' ,'r') as html_email :

    html_content = '\n'.join( html_email.readlines())
    
    html_content = html_content.replace('$DATE' , tdate)
    # html_content = html_content.replace("$USER" , )
    html_content = html_content.replace("$SHARE_LINK", shared_link)


contacts = []
with open ('contacts.txt' , 'r') as csv_file :
    csv_reader = csv.DictReader(csv_file)
    for row in csv_reader :
        contacts.append( (row['name'] , row['email'] ))

for name , email  in contacts :
    
    if msg['To']:
        del msg['To']

    msg['To'] = email
    html_content_user = html_content.replace("$USER", name)
    msg.add_alternative( html_content_user , subtype='html')

    # Send the message
    with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp:
        smtp.login(EMAIL_ADDRESS , EMAIL_PASSWORD)
    
        smtp.send_message(msg)


